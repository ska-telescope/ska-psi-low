Feature: Test digital signal chain

    @forked @XTP-24595 @XTP-24597
    Scenario: Test turning TPM on
        Given a subrack that is ONLINE and ON
        And a tile that is ONLINE and OFF
        When the user turns the tile ON
        Then the subrack reports that the TPM is ON
        And the tile's state is ON
        # long timeout here because TPM init is patchy
        And the tile's tileProgrammingState becomes Initialised within 240s

    @forked @XTP-24595 @XTP-24598
    Scenario: Test turning TPM off
        Given a subrack that is ONLINE and ON
        And a tile that is ONLINE and ON
        When the user turns the tile OFF
        Then the tile's state is OFF
        And the subrack reports that the TPM is OFF
