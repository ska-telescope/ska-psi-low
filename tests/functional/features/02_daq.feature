Feature: Verify TPM data flow

    Background:
        Given a signal generator that is ONLINE and ON
        And the signal generator's frequency is set to 80000000
        And the signal generator's power_dbm is set to 0
        And the signal generator's rf_output_on is set to True
        And a subrack that is ONLINE and ON
        And a tile that is ONLINE and OFF
        And a daq that is ONLINE and ON
        And the tile is ready to send data to the DAQ receiver
        And the DAQ receiver is ready to receive data

    @forked @XTP-24599 @XTP-24595
    Scenario Outline: test DAQ acquisition
        When I send <data_type> data samples from the tile
        Then the DAQ receiver writes a <data_type> data file

        Examples:
            | data_type |
            | raw       |
            | channel   |
