Feature: Verify digital signal chain received at PST

    Background: 
        Given PST is configured for a scan

    # don't upload for now...
    #@XTP-18338 @XTP-16528
    Scenario: test PST acquisition
        When a sine wave signal is fed to one TPM
        Then PST writes the files to the local volume
        And the peak of the tone is reported at the correct channel 
        And other channels have power levels lower than "threshold"