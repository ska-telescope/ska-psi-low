Feature: Verify TPM data flow

    Background:
        Given a signal generator that is ONLINE and ON
        And the signal generator's frequency is set to 152000000
        And the signal generator's power_dbm is set to -36
        And the signal generator's rf_output_on is set to True
        And a P4 switch with an ARP table entry for an unused IP
        And a subrack that is ONLINE and ON
        And a tile that is ONLINE and OFF
        And the tile is configured with the destination IP
        And a cbf-proc that is initialised with the CNIC personality
        And the P4 switch is configured to forward traffic from the tile to the CNIC

    @forked @XTP-24607 @XTP-24595
    Scenario: test CNIC acquisition
        When I configure the tile's station beamformer
        And I instruct the CNIC to capture traffic to a pcap file
        Then a pcap file is written to the test data volume
