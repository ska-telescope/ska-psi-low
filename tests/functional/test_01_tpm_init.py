"""This module contains BBD tests of the digital signal chain."""

from typing import Callable

from pytest_bdd import parsers, scenarios, then
from tango import DeviceProxy

from .conftest import check_tango_device_attribute_value


@then(
    parsers.parse("the subrack reports that the TPM is {power_state}"),
)
def check_subrack_report_tpm_onoff(
    get_device: Callable[[str], DeviceProxy],
    tpm_number: int,
    power_state: str,
) -> None:
    """
    Check that the subrack thinks the TPM is on.

    :param get_device: a caching Tango device factory
    :param tpm_number: number of the subrack bay housing the TPM under
        test
    :param power_state: the expected power state of the TPM
    """
    check_tango_device_attribute_value(
        get_device,
        "subrack",
        f"tpm{tpm_number}PowerState",
        power_state,
    )


scenarios("./features/01_tpm_init.feature")
