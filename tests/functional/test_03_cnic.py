"""This module contains BBD tests of basic DAQ functionality."""

import json
import os
import time
from collections.abc import Callable
from datetime import datetime

import pytest
import tango
from pytest_bdd import given, scenario, then, when

from .conftest import expect_attribute

SPEAD_PACKET_BYTES = 8306

N_RECV_PACKETS = 8


@scenario("./features/03_cnic.feature", "test CNIC acquisition")
def test_cnic_acquisition(
    get_device: Callable[[str], tango.DeviceProxy],
    tpm_number: int,
) -> None:
    """
    Power down all TPMs after running the CNIC scenario.

    :param get_device: a caching Tango device factory
    :param tpm_number: the slot ID of the TPM under test
    """
    subrack = get_device("subrack")
    subrack.PowerOffTpm(tpm_number)


@given(
    "a cbf-proc that is initialised with the CNIC personality",
    target_fixture="cbf_proc",
)
def _initialise_cnic(
    get_device: Callable[[str], tango.DeviceProxy],
    cnic_firmware_version: str,
) -> tango.DeviceProxy:
    # Get ourselves an LowCbfProcessor instance initialised with the CNIC
    # personality. There are a few question marks around this process - see
    # the TODOs below and https://skao.slack.com/archives/CG67W1FCZ/p1677550371298149
    proc = get_device("cbf-proc")
    print("Selecting CNIC personality")
    proc.set_timeout_millis(60_000)
    proc.SelectPersonality(
        json.dumps(
            {
                "personality": "cnic",
                "source": "gitlab",
                "version": cnic_firmware_version,
                "platform": 3,
            }
        )
    )

    # TODO: shouldn't we do this before? Why AFTER loading the personality?
    if proc.simulationMode:
        proc.simulationMode = 0
        time.sleep(5)  # TODO: get rid of this sleep

    if proc.adminMode:
        proc.adminMode = 0

    # TODO: can we not poll+sleep here?
    # wait for dynamic attrs to be available
    for attempts_left in reversed(range(10)):
        try:
            # setting adminMode to 0 makes this (and other) attrs appear
            _ = proc.system__time_uptime
            break
        except tango.DevFailed:
            if attempts_left:
                time.sleep(2)
            else:
                raise

    return proc


@given("the P4 switch is configured to forward traffic from the tile to the CNIC")
def _configure_p4_ports(
    cbf_conn: tango.DeviceProxy,
    cbf_proc: tango.DeviceProxy,
    tpm: tango.DeviceProxy,
    cbf_port_mapping: dict[str, str],
    tpm_port_mapping: dict[str, str],
) -> None:
    configured_ports = {
        port_info["$PORT_NAME"]: port_info
        for port_info in json.loads(cbf_conn.portStatus)["Ports_Status"]
    }

    # Configure the cbf-proc port on the P4. If it's configured already,
    # remove it and re-add to make sure it's in the state we expect.
    cbf_proc_port = cbf_port_mapping[cbf_proc.serialNumber]
    if cbf_proc_port in configured_ports:
        cbf_conn.RemovePorts(json.dumps({"Physical": [{"port": cbf_proc_port}]}))
    cbf_conn.LoadPorts(
        json.dumps(
            {
                "Physical": [
                    {
                        "port": cbf_proc_port,
                        "speed": "100G",
                        "fec": "none",
                        "autoneg": "disable",
                        "mac": "84:c7:8f:03:79:b2",
                    },
                ]
            }
        )
    )

    # Configure the TPM port on the P4. If it's configured already,
    # remove it and re-add to make sure it's in the state we expect.
    tpm_port = tpm_port_mapping[tpm.name()]
    if tpm_port in configured_ports:
        cbf_conn.RemovePorts(json.dumps({"Physical": [{"port": tpm_port}]}))
    cbf_conn.LoadPorts(
        json.dumps(
            {
                "Physical": [
                    {
                        "port": tpm_port,
                        "speed": "40G",
                        "fec": "none",
                        "autoneg": "disable",
                        "mac": "84:c7:8f:03:79:b2",
                    },
                ]
            }
        )
    )

    # The basic routing table is the simplest form of routing on the P4.
    # Each input port is associated with a single output port, and all packets
    # received on the input port are propagated to the output port.
    def get_basic_table() -> dict[str, str]:
        return {
            r["ingress port"]: r["port"]
            for r in json.loads(cbf_conn.basicRoutingTable)["Basic"]
        }

    # Check if the correct entry already exists in the basic routing table.
    # If it doesn't, and there is an existing entry mapping our tpm port to
    # some other port, delete that entry before adding the correct entry.
    existing_dest_port = get_basic_table().get(tpm_port)
    if existing_dest_port != cbf_proc_port:
        if existing_dest_port:
            cbf_conn.RemoveBasicEntry(
                json.dumps({"basic": [{"src": {"port": tpm_port}}]})
            )
        cbf_conn.AddBasicEntry(
            json.dumps(
                {
                    "basic": [
                        {
                            "src": {"port": tpm_port},
                            "dst": {"port": cbf_proc_port},
                        }
                    ]
                }
            )
        )

        assert get_basic_table()[tpm_port] == cbf_proc_port


@given("the tile is configured with the destination IP", target_fixture="tpm")
def _configure_tpm_destination_ip(
    get_device: Callable[[str], tango.DeviceProxy],
    destination_ip: str,
) -> tango.DeviceProxy:
    tpm = get_device("tile")

    # TPM will become `Initialised` some time after being turned on. Sometimes
    # this is fast, but other times it's slow due to a TPM firmware bug:
    # https://jira.skatelescope.org/browse/PRTS-248
    tpm.On()
    assert expect_attribute(tpm, "tileProgrammingState", "Initialised", timeout=240.0)

    # After calling StartAcquisition, the TPM will become `Synchronised`.
    [result_code], [result] = tpm.StartAcquisition("{}")
    print(f"tile.StartAcquisition result: {result_code}; {result}")
    assert expect_attribute(tpm, "tileProgrammingState", "Synchronised")

    # To set the destination_ip for an interface, it has to be set as part of
    # a larger config object. So we fetch the current config, set the
    # destination_ip, and re-submit it.
    fortyg_config = json.loads(tpm.Get40GCoreConfiguration('{"core_id": 1}'))
    fortyg_config["destination_ip"] = destination_ip
    tpm.Configure40GCore(json.dumps(fortyg_config))

    # confirm that we read the right value back
    fortyg_config = json.loads(tpm.Get40GCoreConfiguration('{"core_id": 1}'))
    assert fortyg_config["destination_ip"] == destination_ip

    return tpm


@given(
    "a P4 switch with an ARP table entry for an unused IP", target_fixture="cbf_conn"
)
def _get_cbf_connector(
    get_device: Callable[[str], tango.DeviceProxy],
    destination_ip: str,
) -> tango.DeviceProxy:
    # Get a handle to a P4 switch via LowCbfConnector. Ideally we wouldn't
    # have to call ConnectToSwitch - would probably be nicer for the IP to be
    # a property of the device, and for the device to connect when adminMode
    # was set to ONLINE.
    cbf_conn = get_device("p4")
    cbf_conn.ConnectToSwitch('{"Switch": "202.9.15.135:50052"}')

    # Check that an entry exists in the ARP table for our destination IP. This
    # allows the P4 to respond with an MAC address when the TPM sends an ARP
    # request, which is a prerequisite for the TPM to send data. We don't care
    # what the MAC address is.
    arp_table = {
        entry["IP"]: entry["Mac"]
        for entry in json.loads(cbf_conn.arpRoutingTable)["ARP"]
    }
    if destination_ip not in arp_table:
        arp_entry = {
            "ip": {"address": destination_ip},
            "mac": {"address": "aa:bb:cc:dd:ee:ff"},
        }
        cbf_conn.AddARPEntry(json.dumps({"arp": [arp_entry]}))

    return cbf_conn


@pytest.fixture(scope="session", name="destination_ip")
def _destination_ip() -> str:
    return "192.168.1.1"


@when("I configure the tile's station beamformer")
def _configure_tpm_beamformer(
    tpm: tango.DeviceProxy,
) -> None:
    # This set of magical incantations is what we found got us a decent signal
    # when developing this test in the Jupyter notebooks. This sequence will
    # likely change in later revisions of MccsTile.
    tpm.StopBeamformer()
    tpm.ConfigureStationBeamformer(
        json.dumps(
            {
                "start_channel": 192,
                "n_channels": 8,
                "is_first": True,
                "is_last": True,
            }
        )
    )
    tpm.channeliserRounding = [0]
    tpm.cspRounding = [0]
    tpm.SetBeamFormerRegions([192, 8, 0, 0, 0, 0, 0, 0])
    tpm.StartBeamformer("{}")


@when(
    "I instruct the CNIC to capture traffic to a pcap file",
    target_fixture="pcap_filename",
)
def _cnic_capture_pcap(
    cbf_proc: tango.DeviceProxy,
) -> str:
    # Call the receive_pcap method defined in the CNIC personality to capture
    # SPEAD packets to a .pcap file, and store them on the test data SSD.
    filename_parts = [
        "ci_test",
        os.environ.get("TEST_RUN_ID"),
        datetime.now().isoformat(),
    ]
    pcap_filename = "Viola/" + "-".join(x for x in filename_parts if x) + ".pcap"

    pcap_call = {
        "method": "receive_pcap",
        "arguments": {
            "out_filename": f"/test-data/{pcap_filename}",
            "packet_size": SPEAD_PACKET_BYTES,
            "n_packets": N_RECV_PACKETS,
        },
    }
    cbf_proc.CallMethod(json.dumps(pcap_call))

    return pcap_filename


@then("a pcap file is written to the test data volume")
def _pcap_file_present(
    pcap_filename: str,
) -> None:
    # TODO: query the cbf-proc device for capture completion instead of polling?
    # See AB's hints at https://skao.slack.com/archives/CG67W1FCZ/p1677550371298149
    for attempts_left in reversed(range(10)):
        try:
            # Note this path is different to when we captured. The test data SSD
            # is mounted at a different path in our k8s-test pod than it is in the
            # LowCbfProcessor pods.
            with open(f"/cbf-data/{pcap_filename}", "rb") as pcap_file:
                assert len(pcap_file.read()) == (
                    24  # pcap header bytes
                    + N_RECV_PACKETS
                    * (16 + SPEAD_PACKET_BYTES)  # pcap packet header bytes = 16
                )
            break
        except FileNotFoundError:
            if attempts_left:
                time.sleep(2)
            else:
                raise
