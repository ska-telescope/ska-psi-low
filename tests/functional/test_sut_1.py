"""This module contains a placeholder BDD test (SUT1.1)."""

import pytest
from pytest_bdd import given, scenario, scenarios, then, when


@given("PST is configured for a scan")
def step_impl() -> None:
    """_summary_.

    :raises RuntimeError: _description_
    """
    raise RuntimeError("No implementation yet")


@when("When a sine wave signal is fed to one TPM")
def step_impl1() -> None:
    """_summary_.

    :raises RuntimeError: _description_
    """
    raise RuntimeError("No implementation yet")


@then("PST writes the files to the local volume")
def step_impl2() -> None:
    """_summary_.

    :raises RuntimeError: _description_
    """
    raise RuntimeError("No implementation yet")


@pytest.mark.xfail(reason="BBD test scenario undefined")
@scenario("features/sut_1.feature", "test PST acquisition")
def _test_pst_acquisition() -> None:
    pass


scenarios("features/sut_1.feature")
