"""This module contains BBD tests of basic DAQ functionality."""

import json
import time
from collections.abc import Callable

from pytest_bdd import given, parsers, scenarios, then, when
from tango import DeviceProxy

from .conftest import expect_attribute, wait_attribute


@given("the tile is ready to send data to the DAQ receiver", target_fixture="tpm")
def _prepare_tile(
    get_device: Callable[[str], DeviceProxy],
) -> DeviceProxy:
    tpm = get_device("tile")

    # TPM will become Initialised some time after being turned on
    tpm.On()
    assert expect_attribute(tpm, "tileProgrammingState", "Initialised", timeout=240.0)

    # After StartAcquisition, the TPM will become "Synchronised"
    [result_code], [result] = tpm.StartAcquisition("{}")
    print(f"tile.StartAcquisition result: {result_code}; {result}")
    assert expect_attribute(tpm, "tileProgrammingState", "Synchronised")

    daq = get_device("daq")
    daq_status = json.loads(daq.DaqStatus())
    tpm_lmc_config = {
        "mode": "1G",
        "destination_ip": daq_status["Receiver IP"][0],
        "destination_port": daq_status["Receiver Ports"][0],
    }
    print(f"{tpm_lmc_config=}")

    [result_code], [result] = tpm.SetLmcDownload(json.dumps(tpm_lmc_config))
    print(f"{result_code}: {result}")

    return tpm


@given("the DAQ receiver is ready to receive data", target_fixture="daq")
def _prepare_daq_receiver(
    get_device: Callable[[str], DeviceProxy],
) -> DeviceProxy:
    daq = get_device("daq")

    [result_code], [result] = daq.Configure(
        json.dumps({"directory": "/daq-data", "nof_tiles": 1})
    )
    print(f"daq.Configure result: {result_code}; {result}")

    [result_code], [result] = daq.Start(json.dumps({"modes_to_start": [0, 1]}))
    print(f"daq.Start result: {result_code}; {result}")

    return daq


@when(parsers.parse("I send {data_type} data samples from the tile"))
def _send_data_samples(tpm: DeviceProxy, data_type: str) -> None:
    assert wait_attribute(tpm, "pendingDataRequests", False)
    tpm.SendDataSamples(
        json.dumps(
            {
                "data_type": data_type,
            }
        )
    )


@then(parsers.parse("the DAQ receiver writes a {data_type} data file"))
def _wait_for_daq_file(daq: DeviceProxy, data_type: str) -> None:
    attempts = 10
    time.sleep(5)
    while daq.dataReceivedResult == ("", "") and attempts:
        time.sleep(2)
        attempts -= 1

    daq_result = daq.dataReceivedResult
    print(daq_result)

    assert daq_result[0] == f"burst_{data_type}"


scenarios("./features/02_daq.feature")
