"""Test harness for PSI functional tests."""

import queue
import time
from functools import lru_cache
from typing import Any, Callable, Generator, TypedDict

import pytest
import tango
from pytest_bdd import given, parsers, then, when
from ska_control_model import AdminMode, ResultCode
from ska_tango_testing.context import TangoContextProtocol, TrueTangoContextManager
from ska_tango_testing.mock.placeholders import OneOf
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

SubrackInfoType = TypedDict(
    "SubrackInfoType", {"host": str, "port": int, "simulator": bool}
)

DeviceMapping = TypedDict("DeviceMapping", {"name": str, "subscriptions": list[str]})


@pytest.fixture(name="device_mapping", scope="session")
def device_mapping_fixture(tpm_number: int) -> dict[str, DeviceMapping]:
    """
    Return a mapping from short to canonical Tango device names.

    :param tpm_number: the sequence number of the TPM in use

    :return: a map of short names to full Tango device names of the form
        "<domain>/<class>/<instance>"
    """
    return {
        "signal generator": {
            "name": "psi-low/siggen/1",
            "subscriptions": [
                "adminMode",
                "state",
                # "longRunningCommandStatus",
            ],
        },
        "subrack": {
            "name": "low-mccs/subrack/psi-low-1",
            "subscriptions": [
                "adminMode",
                "state",
                f"tpm{tpm_number}PowerState",
            ],
        },
        "tile": {
            "name": f"low-mccs/tile/psi-low-{tpm_number:02}",
            "subscriptions": [
                "adminMode",
                "state",
                "tileProgrammingState",
            ],
        },
        "daq": {
            "name": "low-mccs/daqreceiver/psi-low",
            "subscriptions": [
                "adminMode",
                "state",
            ],
        },
        "p4": {
            "name": "tango-databaseds.ska-low-cbf-conn:10000/low-cbf/connector/0",
            "subscriptions": [
                "adminMode",
                "state",
            ],
        },
        "cbf-proc": {
            "name": "low-cbf/processor/0.0.0",
            "subscriptions": [
                "adminMode",
                "state",
            ],
        },
    }


@pytest.fixture(name="get_device", scope="session")
def get_device_fixture(
    tango_context: TangoContextProtocol,
    device_mapping: dict[str, DeviceMapping],
    change_event_callbacks: MockTangoEventCallbackGroup,
) -> Callable[[str], tango.DeviceProxy]:
    """
    Return a memoized function that returns a DeviceProxy for a given name.

    :param tango_context: a TangoContextProtocol to instantiate DeviceProxys
    :param device_mapping: a map from short to canonical device names
    :param change_event_callbacks: dictionary of mock change event
        callbacks with asynchrony support

    :return: a memoized function that takes a name and returns a DeviceProxy
    """

    @lru_cache
    def _get_device(short_name: str) -> tango.DeviceProxy:
        device_data = device_mapping[short_name]
        name = device_data["name"]
        tango_device = tango_context.get_device(name)

        # TODO: why do some devices i.e. MccsDaqReceiver need this?
        for _ in range(23):
            try:
                device_info = tango_device.info()
                break
            except tango.DevFailed:
                time.sleep(5)
        else:
            device_info = tango_device.info()

        dev_class = device_info.dev_class
        print(f"Created DeviceProxy for {short_name} - {dev_class} {name}")
        for attr in device_data.get("subscriptions", []):
            attr_value = tango_device.read_attribute(attr).value
            attr_event = change_event_callbacks[f"{name}/{attr}"]
            tango_device.subscribe_event(
                attr,
                tango.EventType.CHANGE_EVENT,
                attr_event,
            )
            print(f"Subscribed to {name}/{attr}")
            attr_event.assert_change_event(attr_value)
            print(f"Received initial value for {name}/{attr}: {attr_value}")

        return tango_device

    return _get_device


@pytest.fixture(name="tango_context", scope="session")
def tango_context_fixture() -> Generator[TangoContextProtocol, None, None]:
    """
    Yield a Tango context containing the device/s under test.

    :yields: a Tango context containing the devices under test
    """
    with TrueTangoContextManager() as context:
        yield context


@pytest.fixture(name="tpm_number", scope="session")
def tpm_number_fixture() -> int:
    """
    Return the number of the TPM under test in the subrack under test.

    :returns: the number of the TPM
    """
    # 20221220: This is not the TPM in the PSI-Low that the signal generator
    # is connected to. That one is in use in the "staging" namespace.
    return 5


@pytest.fixture(name="change_event_callbacks", scope="session")
def change_event_callbacks_fixture(
    device_mapping: dict[str, DeviceMapping],
) -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of change event callbacks with asynchrony support.

    :param device_mapping: a map from short to canonical device names

    :returns: a callback group.
    """
    keys = [
        f"{info['name']}/{attr}"
        for info in device_mapping.values()
        for attr in info["subscriptions"]
    ]

    return MockTangoEventCallbackGroup(
        *keys,
        timeout=30.0,  # TPM takes a long time to initialise
    )


@pytest.fixture
def cbf_port_mapping() -> dict[str, str]:
    """
    Return a mapping from Alveo FPGA serial numbers to P4 port ids.

    :returns: a dict of FPGA serial keys to P4 port id values
    """
    return {
        "XFL1ZIN0F4RO": "42/0",
        "XFL14SLO1LIF": "44/0",
        "XFL1DKXBEVG2": "46/0",
        "XFL1HOOQ1Y44": "48/0",
        "XFL1VCYSXCL0": "50/0",
        "XFL10NIYKVEU": "52/0",
        "XFL1LHN4TXO2": "54/0",
        "XFL1XCRTUC22": "56/0",
        "XFL1E35JVJTQ": "58/0",
        "XFL1RCFEG244": "60/0",
    }


@pytest.fixture
def tpm_port_mapping() -> dict[str, str]:
    """
    Return a mapping from TPM device names to P4 port ids.

    :returns: a dict of TPM device name keys to P4 port id values
    """
    # TODO: this should be based on physical identity not Tango device name
    return {
        "low-mccs/tile/0002": "27/0",
        "low-mccs/tile/0005": "29/0",
    }


@pytest.fixture
def cnic_firmware_version() -> str:
    """
    Return the version of the CNIC firmware we want to use.

    This value is the firmware package corresponding to the 0.1.4 tag, pulled from
    https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-cnic/-/packages.

    :returns: the version number of the desired CNIC firmware
    """
    return "0.1.4-dev.ec712610"


def expect_attribute(
    tango_device: tango.DeviceProxy,
    attr: str,
    value: Any,
    *,
    timeout: float = 60.0,
) -> bool:
    """
    Wait for Tango attribute to have a certain value using a subscription.

    Sets up a subscription to a Tango device attribute,
    waits for the attribute to have the provided value within a given time,
    then removes the subscription.

    :param tango_device: a DeviceProxy to a Tango device
    :param attr: the name of the attribute to be monitored
    :param value: the attribute value we're waiting for
    :param timeout: the maximum time to wait, in seconds
    :return: True if the attribute has the expected value within the given timeout
    """
    print(f"Expecting {tango_device.dev_name()}/{attr} == {value!r} within {timeout}s")
    _queue: queue.SimpleQueue[tango.EventData] = queue.SimpleQueue()
    subscription_id = tango_device.subscribe_event(
        attr,
        tango.EventType.CHANGE_EVENT,
        _queue.put,
    )
    deadline = time.time() + timeout
    try:
        while True:
            event = _queue.get(timeout=deadline - time.time())
            print(f"Got {tango_device.dev_name()}/{attr} == {event.attr_value.value!r}")
            if event.attr_value.value == value:
                return True
    finally:
        tango_device.unsubscribe_event(subscription_id)


def wait_attribute(
    tango_device: tango.DeviceProxy,
    attr: str,
    value: Any,
    *,
    timeout: float = 60.0,
) -> bool:
    """
    Poll a Tango attribute, up to a timeout, until it has the given value.

    If the attribute supports subscriptions, use expect_attribute instead.

    :param tango_device: a DeviceProxy to a Tango device
    :param attr: the name of the attribute to be monitored
    :param value: the attribute value we're waiting for
    :param timeout: the maximum time to wait, in seconds
    :return: True if the attribute has the expected value within the given timeout
    """
    print(
        f"Polling for {tango_device.dev_name()}/{attr} == {value!r} within {timeout}s"
    )
    deadline = time.time() + timeout
    while time.time() < deadline:
        current_value = tango_device.read_attribute(attr).value
        print(f"Got {tango_device.dev_name()}/{attr} == {current_value!r}")
        if current_value == value:
            return True
        time.sleep(1.0)
    return False


def _str_to_tango(attr: tango.AttributeInfoEx, value: str) -> Any:
    """
    Convert a str to a type compatible with the given Tango attribute.

    :param attr: metadata about the attribute being compared against
    :param value: a string value from a parametrised BDD step
    :return: a value that can be compared to the attribute's value
    """
    print(f"coerceing value {value} for attr {attr.name}")
    retval: Any
    if attr.data_type in [
        tango.ArgType.DevLong,
        tango.ArgType.DevLong64,
        tango.ArgType.DevShort,
        tango.ArgType.DevULong,
        tango.ArgType.DevULong64,
        tango.ArgType.DevUShort,
    ]:
        retval = int(value)

    elif attr.data_type in [
        tango.ArgType.DevDouble,
        tango.ArgType.DevFloat,
    ]:
        retval = float(value)

    elif attr.data_type == tango.ArgType.DevBoolean:
        retval = value in ["True", "true"]

    elif attr.data_type == tango.ArgType.DevState:
        retval = getattr(tango.DevState, value)

    elif attr.data_type == tango.ArgType.DevEnum:
        # StdStrVector doesn't have an index() method
        retval = next(
            (i for i, v in enumerate(attr.enum_labels) if v == value),
            object(),  # nothing will have this value
        )
    elif attr.data_type == tango.ArgType.DevString:
        retval = value
    else:
        print(f"Couldn't coerce {repr(value)}")
        retval = value
    return retval


_DevState = {"ON": tango.DevState.ON, "OFF": tango.DevState.OFF}.__getitem__


@given(
    parsers.parse("a {short_name} that is {mode} and {state}"),
    converters={
        "state": _DevState,
        "mode": AdminMode.__getitem__,
    },
)
def get_online_tango_device(
    change_event_callbacks: MockTangoEventCallbackGroup,
    get_device: Callable[[str], tango.DeviceProxy],
    short_name: str,
    mode: AdminMode,
    state: tango.DevState,
) -> tango.DeviceProxy:
    """
    Given a short name, get a Tango device in the given state and mode.

    :param change_event_callbacks: dictionary of mock change event
        callbacks with asynchrony support
    :param get_device: a caching Tango device factory
    :param short_name: the short name for the Tango device
    :param mode: the desired AdminMode
    :param state: the desired DevState
    :return: a Tango DeviceProxy to a device in the desired state
    """
    dev = get_device(short_name)
    dev_name = dev.dev_name()

    initial_admin_mode = dev.read_attribute("adminMode").value
    admin_mode_events = change_event_callbacks[f"{dev_name}/adminMode"]

    initial_state = dev.read_attribute("state").value
    state_events = change_event_callbacks[f"{dev_name}/state"]

    # assert that state is what it should be given the AdminMode
    if initial_admin_mode in {AdminMode.ONLINE, AdminMode.ENGINEERING}:
        # TODO: TPM simulator sometimes goes into ALARM due to MIN CURRENT
        assert initial_state in {
            tango.DevState.OFF,
            tango.DevState.ALARM,
            tango.DevState.ON,
        }
    else:  # AdminMode OFFLINE, NOT_FITTED, RESERVED
        assert initial_state == tango.DevState.DISABLE

    # only support ONLINE for now
    assert mode == AdminMode.ONLINE

    # bring ONLINE if not already
    if initial_admin_mode != AdminMode.ONLINE:
        dev.adminMode = AdminMode.ONLINE
        admin_mode_events.assert_change_event(AdminMode.ONLINE)

        if initial_admin_mode == AdminMode.ENGINEERING:
            state_events.assert_not_called()
        else:
            # TODO: MccsTile should transition to UNKNOWN but doesn't
            # if dev.info().dev_class != "MccsTile":
            state_events.assert_change_event(tango.DevState.UNKNOWN)
            state_events.assert_change_event(
                OneOf(tango.DevState.ON, tango.DevState.OFF)
            )

    # should we be on or off?
    if dev.read_attribute("state").value != state:
        print(f"Turning {dev.dev_name()} {state}")
        turn_tango_device_onoff(change_event_callbacks, get_device, short_name, state)

    return dev


@given(parsers.parse("the {device}'s {attribute} is set to {value}"))
@when(parsers.parse("the {device}'s {attribute} is set to {value}"))
def set_tango_device_attribute(
    get_device: Callable[[str], tango.DeviceProxy],
    device: str,
    attribute: str,
    value: str,
) -> None:
    """
    Set a Tango device's attribute to the given value.

    Write the attribute value using DeviceProxy.write_attribute, and confirm that it
    takes that value using expect_attribute. Before writing the value, it's converted
    to the correct Tango type based on the attribute's metadata.

    :param get_device: a caching Tango device factory
    :param device: the short name for the Tango device
    :param attribute: the name of the attribute to be written
    :param value: the value to write to the attribute
    """
    dev = get_device(device)
    attr_config = dev.attribute_query(attribute)
    tango_value = _str_to_tango(attr_config, value)
    dev.write_attribute(attribute, tango_value)
    expect_attribute(dev, attribute, tango_value)


@when(
    parsers.parse("the user turns the {short_name} {desired_state}"),
    converters={"desired_state": _DevState},
)
def turn_tango_device_onoff(
    change_event_callbacks: MockTangoEventCallbackGroup,
    get_device: Callable[[str], tango.DeviceProxy],
    short_name: str,
    desired_state: tango.DevState,
) -> None:
    """
    Turn a Tango device on or off using its On() and Off() commands.

    :param change_event_callbacks: dictionary of mock change event
        callbacks with asynchrony support
    :param get_device: a caching Tango device factory
    :param short_name: the short name of the device
    :param desired_state: the desired power state, either "on" or "off"
    """
    dev = get_device(short_name)
    # Issue the command
    if desired_state == tango.DevState.ON:
        [result_code], [command_id] = dev.On()
    else:
        [result_code], [command_id] = dev.Off()
    assert result_code == ResultCode.QUEUED
    print(f"Command queued on {dev.dev_name()}: {command_id}")

    # while not (
    #     result_code == "COMPLETED"
    #     or (
    #         # status to FAILED even when it succeeds in turning its TPM on
    #         dev.info().dev_class == "MccsTile"
    #         and result_code == "FAILED"
    #     )
    # ):
    #     call_details = change_event_callbacks[
    #         f"{dev.dev_name()}/longRunningCommandStatus"
    #     ].assert_against_call()
    #     print(f"LRCS on {dev.dev_name()}: {call_details['attribute_value']}")
    #     assert call_details["attribute_value"][-2] == command_id
    #     result_code = call_details["attribute_value"][-1]

    change_event_callbacks[f"{dev.dev_name()}/state"].assert_change_event(desired_state)


@then(parsers.parse("the {device}'s {attribute} is {value}"))
def check_tango_device_attribute_value(
    get_device: Callable[[str], tango.DeviceProxy],
    device: str,
    attribute: str,
    value: str,
) -> None:
    """
    Check that a Tango attribute has a particular value.

    :param get_device: a caching Tango device factory
    :param device: short device name
    :param attribute: the name of the attribute to check
    :param value: the attribute value to assert against
    """
    dev = get_device(device)
    attr_config = dev.attribute_query(attribute)
    cmp_value = _str_to_tango(attr_config, value)
    print(f"value: {value}\ncoerced: {cmp_value}")
    attr = dev.read_attribute(attribute)
    print(f"{dev.dev_name()}/{attribute}:")
    print(attr)
    assert attr.value == cmp_value


# TODO: see comment below - can't make timeout optional without fixing pytest-bdd
# @then(parsers.parse("the {device}'s {attribute} becomes {value}"))
@then(parsers.parse("the {device}'s {attribute} becomes {value} within {timeout:g}s"))
def check_tango_device_attribute_change_event_timeout(
    get_device: Callable[[str], tango.DeviceProxy],
    device: str,
    attribute: str,
    value: str,
    timeout: float,  # setting a default over overrides any parsed value
) -> None:
    """
    Watch Tango attribute change events until an expected value is seen.

    :param get_device: a caching Tango device factory
    :param device: short device name
    :param attribute: the name of the attribute to check
    :param value: the attribute value to assert against
    :param timeout: how long to wait for the attribute value to change
    """
    dev = get_device(device)

    attr_meta = dev.attribute_query(attribute)
    cmp_value = _str_to_tango(attr_meta, value)

    assert expect_attribute(dev, attribute, cmp_value, timeout=timeout)


@then(
    parsers.parse("the {device} reports that it is {state}"),
    converters={"state": _DevState},
)
def check_tango_device_state(
    get_device: Callable[[str], tango.DeviceProxy],
    device: str,
    state: tango.DevState,
) -> None:
    """
    Check that a device is in a given DevState.

    :param get_device: a caching Tango device factory
    :param device: short device name
    :param state: the expected tango.DevState value
    """
    dev = get_device(device)
    assert dev.read_attribute("state").value == state
