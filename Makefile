-include PrivateRules.mak

include .make/base.mk

include .make/xray.mk

########################################################################
# PYTHON
########################################################################

PYTHON_LINE_LENGTH = 88
PYTHON_SWITCHES_FOR_BLACK =
PYTHON_SWITCHES_FOR_ISORT =

include .make/python.mk

# https://github.com/pytest-dev/pytest-bdd/issues/401
PYTHON_VARS_BEFORE_PYTEST = PYTHONDONTWRITEBYTECODE=True

python-post-lint:
	mypy --config-file mypy.ini tests/

.PHONY: python-post-lint

########################################################################
# DOCS
########################################################################

include .make/docs.mk

DOCS_SPHINXOPTS = -W --keep-going

docs-pre-build:
	poetry config virtualenvs.create false
	poetry install --no-root --only docs

.PHONY: docs-pre-build


########################################################################
# OCI
########################################################################

include .make/oci.mk

# We only build a test image, which doesn't need to be pushed to the CAR
OCI_IMAGES_TO_PUBLISH =

# cache from the most recently-built dev image on Gitlab. This won't always
# be the most recent build on the same branch, but this is simple to implement
# and in most cases will be a win.
ifneq ($(CI_JOB_ID),)
OCI_BUILD_ADDITIONAL_ARGS = --cache-from $(CI_REGISTRY_IMAGE)/ska-psi-low:$(VERSION)
endif

########################################################################
# Helm
########################################################################

include .make/helm.mk

# only publish main chart not test parent
HELM_CHARTS_TO_PUBLISH=

deploy-psi-low: K8S_UMBRELLA_CHART_PATH=charts/ska-psi-low
deploy-psi-low: HELM_RELEASE=test  # stupid release name but there's a PV that requires it
deploy-psi-low: KUBE_NAMESPACE=ska-psi-low
deploy-psi-low: K8S_CHARTS=ska-psi-low
deploy-psi-low: k8s-install-chart


########################################################################
# K8S
########################################################################

include .make/k8s.mk

k8s-pre-test:
	poetry export --only dev --without-hashes > tests/requirements.txt

########################################################################
# Config capture
########################################################################

# The project to store the configuration in
# needs token set up for this project
CONFIG_CAPTURE_PROJECT = ska-psi-low-config

include .make/configcapture.mk

CONFIG_CAPTURE_ATTRIBUTES = '["versionId", "buildState", "serialNumber", "firmwareVersion"]'

########################################################################
# XRAY
########################################################################

include .make/xray.mk

########################################################################

DEPLOYMENT_CONTEXT ?= PSI-Low

# Chart for testing
ifeq ($(shell kubectl config current-context),minikube)
MINIKUBE ?= true
else
MINIKUBE ?= false
endif

ifneq ($(CI_JOB_ID),)
# For k8s-install-chart
_gitlab_image_tag = $(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
_gitlab_registry = $(CI_REGISTRY_IMAGE)
else
# If we're running locally, use the most recent image from GitLab
_gitlab_image_tag = $(VERSION)-dev.c$(shell git rev-parse --short=8 @{u})
_gitlab_registry = registry.gitlab.com/$(shell git remote -v | head -1 | grep -oE 'ska-telescope(/[a-z0-9-]{1,}){1,}')
endif

K8S_CHART_PARAMS += \
	--set global.minikube=$(MINIKUBE) \
 	--set ska-taranta.enabled=false  # taranta charts are slow to terminate

PYTHON_VARS_AFTER_PYTEST = \
	-v \
	--cucumberjson=build/reports/cucumber.json \
	--json-report --json-report-file=build/reports/report.json

TANGO_HOST ?= $(shell helm get -n $(KUBE_NAMESPACE) values -a $(HELM_RELEASE) -o json | jq -r '.global.tango_host')

k8s-do-test:
	helm -n $(KUBE_NAMESPACE) upgrade --install k8s-test charts/k8s-test \
		--set image.registry="$(_gitlab_registry)" \
		--set image.tag="$(_gitlab_image_tag)" \
		--set env.TANGO_HOST="$(TANGO_HOST)" \
		--set env.http_proxy="$(http_proxy)" \
		--set env.https_proxy="$(https_proxy)" \
		--set env.no_proxy="$(shell sed 's/,/\\,/g' <<< '$(no_proxy)')" \
		--set env.TEST_RUN_ID="$(CI_JOB_ID)"
	kubectl -n $(KUBE_NAMESPACE) wait pod k8s-test-runner --for=condition=ready --timeout=120s
	kubectl -n $(KUBE_NAMESPACE) cp tests/ k8s-test-runner:tests
	@kubectl -n $(KUBE_NAMESPACE) exec k8s-test-runner -- bash -c "\
		mkdir -p build/reports && \
		pip install -r tests/requirements.txt && \
		pytest $(PYTHON_VARS_AFTER_PYTEST) tests" ; \
	EXIT_CODE=$$? ; \
	kubectl -n $(KUBE_NAMESPACE) cp k8s-test-runner:build/ ./build/ ; \
	helm -n $(KUBE_NAMESPACE) uninstall k8s-test ; \
	exit $$EXIT_CODE
