FROM artefact.skao.int/ska-tango-images-pytango-builder:9.3.35

RUN poetry config virtualenvs.create false

COPY pyproject.toml poetry.lock ./

RUN poetry self update && poetry install --only dev
