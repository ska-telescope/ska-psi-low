# ska-psi-low

This project contains resources for integration and testing in PSI Low. This includes

* a Helm chart deploying the LOW signal chain
* BDD scenarios to exercise signal chain components
* Jupyter notebooks implementing test scenarios

The end goal of this repo is to deploy and test the LOW signal chain as described in https://confluence.skatelescope.org/display/SE/LOW+SUT1+digital+signal+chain.

## Deploying the ska-psi-low chart

The ska-psi-low chart is designed to be deployed against the PSI Low hardware. Other deployment targets, e.g. minikube, might work with some editing of values files, but are not supported.

Before you can deploy, you'll need access to the PSI. If you don't have it, you can find [instructions for obtaining
access](https://confluence.skatelescope.org/display/SWSI/PSI+Low+Deployment+and+Operations#PSILowDeploymentandOperations-Accesstothecluster)
on Confluence. That guide also covers setting up your local kubeconfig to point to the PSI Low cluster.

To deploy to PSI Low, you can simply run `make k8s-install-chart`. The chart's values are already correct for deployment in PSI Low. This will deploy into the namespace `ska-psi-low` by default;
use the `KUBE_NAMESPACE` var to override this.

## Manually running the BDD tests

Once you have deployed, you can run the tests with `make k8s-test`. You need to specify some extra variables here, namely:

* `PROXY_VALUES`, so that the PSI's HTTP proxy is available to the test runner.
* `K8S_TEST_IMAGE_TO_TEST`, because the Makefile machinery will by default assume that an image is built for the current
  repository, but we don't build any image.

The final command looks like:

```
make k8s-test PROXY_VALUES="--env=https_proxy=http://delphoenix.atnf.csiro.au:8888" K8S_TEST_IMAGE_TO_TEST="artefact.skao.int/ska-tango-images-pytango-runtime:9.3.22"
```

## Interacting with the deployed system

You can play with the Tango environment using the itango pod that's deployed as part of the ska-tango-base subchart.
This will open an ITango console, where you can obtain a DeviceProxy by typing `Device("` and then hitting <tab> to
autocomplete from the list of deployed Tango devices.

```
me@local:~/ska-psi-low$ kubectl -n ska-psi-low exec --stdin --tty ska-tango-base-itango-console -- itango3
Defaulted container "itango" out of: itango, check-dependencies-0 (init)
ITango 9.3.3 -- An interactive Tango client.

Running on top of Python 3.10.6, IPython 8.5 and PyTango 9.3.3

help      -> ITango's help system.
object?   -> Details about 'object'. ?object also works, ?? prints more.

IPython profile: tango

hint: Try typing: mydev = Device("<tab>

In [1]: subrack = Device("
                           low-mccs/subrack/0001   psi-low/siggen/1        sys/rest/0             
                           low-mccs/tile/0005      sys/access_control/1    sys/tg_test/1          
                           psi-low/daqreceiver/001 sys/database/2                                 
```

